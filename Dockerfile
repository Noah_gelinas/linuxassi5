# Main container components
FROM httpd:2.4
COPY ./app/assignment-4-web-nasa /usr/local/apache2/htdocs/

# Setting Working Directory
WORKDIR .

# SETTING LABELS
LABEL "author"="Noah Gelinas"
LABEL "app-type"="website"
